import { Component, Input, TemplateRef } from '@angular/core';

@Component({
    selector: 'app-datagrid-type2',
    template: `
    <table class="table table-hover table-bordered">
        <thead>
            <tr *ngIf="data">
                <th *ngFor="let header of config">{{(header.header || header.key) | uppercase}}</th>
                <th>ACTIONS</th>
            </tr>
        </thead>
        <tbody>
            <tr *ngFor="let model of data">
                <td *ngFor="let item of config">
                    {{model[item.key]}}
                </td>
                <td>
                    <ng-container *ngTemplateOutlet="actions; context: { $implicit: model }"></ng-container>
                </td>
            </tr>
        </tbody>
    </table>
    `
})

export class DataGridType2Component {
    @Input() data: any[];
    @Input() config;
    @Input() actions: TemplateRef<any>;
}
