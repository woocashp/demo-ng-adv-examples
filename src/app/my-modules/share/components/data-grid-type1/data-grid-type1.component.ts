import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-data-grid-type1',
    template: `
        <table class="table table-hover table-sm">
            <thead>
                <tr><th *ngFor="let head of data[0] | keyvalue">{{head.key}}</th></tr>
            </thead>
            <tbody>
                <tr appDataGridType1Row [type]="rowType" [data]="item" *ngFor="let item of data"></tr>
            </tbody>
        </table>
    `
})
export class DataGridComponentType1 {
    @Input() rowType: any;
    @Input() data: any[];
}
