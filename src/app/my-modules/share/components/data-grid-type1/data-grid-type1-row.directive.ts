import { Directive, ViewContainerRef, Input, OnInit, ComponentFactoryResolver } from '@angular/core';

@Directive({
    selector: '[appDataGridType1Row]'
})
export class DataGridType1RowDirective implements OnInit {

    @Input() type: any;
    @Input() data: any;

    constructor(
        public container: ViewContainerRef,
        public resolver: ComponentFactoryResolver) {
    }

    ngOnInit(): void {
        let componentFactory = this.resolver.resolveComponentFactory(this.type);
        let componentRef = this.container.createComponent(componentFactory);
        (<any>componentRef.instance).data = this.data;
    }

}
