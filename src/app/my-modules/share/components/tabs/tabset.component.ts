import { Component, ElementRef, QueryList, AfterContentInit, Input, ContentChildren, ViewEncapsulation, Renderer2 } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'tab',
    template: "<ng-content></ng-content>"
})
export class Tab {
    @Input() title: string;
    active: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(private render: Renderer2, private el: ElementRef) {
        this.active
            .subscribe(result => {
                !result ? this.render.addClass(this.el.nativeElement, 'hide') : this.render.removeClass(this.el.nativeElement, 'hide');
            })
    }
}

@Component({
    selector: 'tabset',
    styleUrls: ["./tabset.component.css"],
    encapsulation: ViewEncapsulation.ShadowDom,
    template: `
        <div class="box">
            <ul>
                <li *ngFor="let tab of tabs"
                    [class.active]="tab.active | async"
                    (click)="setActive(tab)">
                        {{tab.title}}
                </li>
            </ul>
            <div class="content">
                <ng-content></ng-content>
            </div>
        </div>
    `
})
// elements which are used between the opening and closing tags of the host element of a given
// component are called content children. It's why we use AfterContentInit class here.
export class TabsetComponent implements AfterContentInit {
    //Configures a content query.
    //Content queries are set before the ngAfterContentInit callback is called.
    @ContentChildren(Tab) tabs: QueryList<Tab>;

    setActive(tab) {
        this.tabs.toArray().forEach(item => item.active.next(false));
        tab.active.next(true);
    }

    ngAfterContentInit() {
        this.tabs.toArray()[0].active.next(true);
    }
}
