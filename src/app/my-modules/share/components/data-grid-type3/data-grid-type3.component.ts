import { Component, OnInit, Input, ContentChild, TemplateRef } from '@angular/core';

@Component({
    selector: 'data-grid-type3',
    template: `
        <table class="table table-hover table-bordered table-sm">
            <thead>
                <th *ngFor="let h of head">{{h|uppercase}}</th>
            </thead>
            <tbody>
                <ng-template ngFor [ngForOf]='data' [ngForTemplate]='tpl'></ng-template>
            </tbody>
        </table>
    `
})

export class DataGridType3Component {
    @Input() data: any[];
    @Input() head: string[];
    @ContentChild(TemplateRef, { static: true }) tpl;
}
