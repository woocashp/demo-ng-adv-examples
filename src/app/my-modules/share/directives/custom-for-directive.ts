import {
    Directive,
    ViewRef,
    ViewContainerRef,
    TemplateRef,
    DoCheck,
    IterableDiffers,
    IterableDiffer,
    Input,
} from '@angular/core';

@Directive({
    selector: '[customRepeater]',
})

export class CustomRepeaterDirective implements DoCheck {
    private differ: IterableDiffer<any>;
    private views: Map<any, ViewRef> = new Map<any, ViewRef>();
    @Input() customRepeaterOf;

    constructor(
        private viewContainer: ViewContainerRef,
        private template: TemplateRef<any>,
        private differs: IterableDiffers
    ) {
        this.differ = this.differs.find([]).create(null);
    }

    ngDoCheck(): void {
        let changes = this.differ.diff(this.customRepeaterOf);
        if (changes) {
            changes.forEachAddedItem((change) => {
                let view = this.viewContainer.createEmbeddedView(this.template, { '$implicit': change.item });
                this.views.set(change.item, view);
            });
            changes.forEachRemovedItem((change) => {
                let view = this.views.get(change.item);
                let idx = this.viewContainer.indexOf(view);
                this.viewContainer.remove(idx);
                this.views.delete(change.item);
            });
        }
    }
}
