import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { CustomIfDirective } from "./directives/custom-if.directive";
import { CustomRepeaterDirective } from "./directives/custom-for-directive";
import { Tab, TabsetComponent } from "./components/tabs/tabset.component";
import { RowComponentType1, RowComponentType2 } from '../../containers/custom-datagrid-example';
import { DataGridComponentType1 } from "./components/data-grid-type1/data-grid-type1.component";
import { DataGridType1RowDirective } from "./components/data-grid-type1/data-grid-type1-row.directive";
import { DataGridType2Component } from "./components/data-grid-type2/data-grid-type2.component";
import { DataGridType3Component } from "./components/data-grid-type3/data-grid-type3.component";

@NgModule({
    imports: [BrowserModule],
    declarations: [
        CustomIfDirective,
        CustomRepeaterDirective,
        TabsetComponent,
        Tab,
        DataGridComponentType1,
        DataGridType1RowDirective,
        RowComponentType1,
        RowComponentType2,
        DataGridType2Component,
        DataGridType3Component
    ],
    exports: [
        CustomIfDirective,
        CustomRepeaterDirective,
        TabsetComponent,
        Tab,
        DataGridComponentType1,
        DataGridType1RowDirective,
        DataGridType2Component,
        DataGridType3Component
    ]
})

export class ShareModule { }
