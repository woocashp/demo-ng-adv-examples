import { Component } from "@angular/core";

@Component({
  selector: "app",

  template: `

        <div class="card bg-light">
            <div class="card-body">
              <h4>Custom-if example</h4>
              <h6>learn about | services: ViewContainerRef and TemplateRef</h6>
            </div>
        </div>
        <br>
        <div>
          <button class="btn btn-secondary" (click)="toggle()">
            show / hide
          </button>
          <br>
          <!-- * syntax is sugar for the actual template markup-->
          <div *customIfDirective="access" class="card-panel teal text-darken-1">
            content is displayed
          </div>
        </div>`
})

export class CustomIfExampleComponent {
  access: boolean = true;

  toggle() {
    this.access = !this.access;
  }
}
