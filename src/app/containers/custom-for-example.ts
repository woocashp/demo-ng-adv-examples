import { Component, ViewChild } from '@angular/core';

@Component({
    template: `
        <div class="card bg-light">
            <div class="card-body">
                <h4>Custom-for example</h4>
                <h6>learn about | lifecycle ngDoCheck and service IterableDiffer</h6>
            </div>
        </div>
        <br>
        <div class="card card-body bg-light">
            <form #addForm="ngForm" class="form-inline">
                <input required type="text" class="form-control" ngModel name="name" placeholder="name">
                <input required type="text" class="form-control" ngModel name="age" placeholder="age">
                <button class="btn btn-secondary" [disabled]="!addForm.valid" (click)="add(addForm)">Add</button>
            </form>
        </div>
        <br>
        <ul>
            <li *customRepeater="let p of people" class="mb-1">
                {{ p.name }} is {{ p.age }}
                <button class="btn btn-danger" (click)="remove(p)">Remove</button>
            </li>
        </ul>
    `
})
export class CustomForExampleComponent {
    people: any[];
    @ViewChild('addForm', { static: true }) addForm;

    constructor() {
        this.people = [
            { name: 'Joe', age: 10 },
            { name: 'Patrick', age: 21 },
            { name: 'Melissa', age: 12 },
            { name: 'Kate', age: 19 }
        ];
    }

    remove(person) {
        let idx: number = this.people.indexOf(person);
        this.people.splice(idx, 1);
    }

    add({ value }) {
        this.people.push(value);
        this.addForm.reset();
    }
}
