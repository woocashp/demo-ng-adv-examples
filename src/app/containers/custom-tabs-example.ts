import { Component } from '@angular/core';

@Component({
    template: `
        <h4>Custom-tabs</h4>
        <h6>learn about | lifecycle: AfterContentInit; decorator: ContentChildren; services: ElementRef, Renderer</h6>
        <br>
        <tabset>
            <!--nav will be hire-->
            <tab *ngFor="let tab of tabs" [title]="tab.title">
                <div [innerHTML]="tab.content"></div>
            </tab>
            <tab [title]="'Another tab'">
                <img src="/assets/admin.png" width="70" style="float: left; margin-right: 10px;">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut dolore ducimus eveniet, facere, inventore
                ipsum iste iusto magni minima mollitia nam non officia porro praesentium quibusdam quisquam ratione
                veritatis voluptatibus.ur adipisicing elit. Aut dolore ducimus eveniet, facere, inventore
                ipsum iste iusto magni minima mollitia nam non officia porro praesentium quibusdam quisquam ratione
                veritatis voluptatibus.ur adipisicing elit. Aut dolore ducimus eveniet, facere, inventore
                ipsum iste iusto magni minima mollitia nam non officia porro praesentium quibusdam quisquam ratione
                veritatis voluptatibus.ur adipisicing elit. Aut dolore ducimus eveniet, facere, inventore
                ipsum iste iusto magni minima mollitia nam non officia porro praesentium quibusdam quisquam ratione
                veritatis voluptatibus.
            </tab>
        </tabset>
    `
})

export class CustomTabsetExampleComponent {
    tabs: {}[];

    constructor() {
        this.tabs = [
            { title: 'About', content: 'This is the <h1>About</h1> tab' },
            { title: 'Contact us', content: '<h1>Contact</h1> us here' }
        ];
    }
}
