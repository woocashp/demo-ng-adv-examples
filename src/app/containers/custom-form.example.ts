import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-custom-form',
  templateUrl: './custom-form.example.html',
  styles: [`label {font-weight: bold;}`]
})
export class CustomFormComponent implements OnInit {

  form: FormGroup;

  ngOnInit() {
    this.form = new FormGroup({
      username: new FormControl('joe'),
      myselect: new FormControl(2)
    });
  }

}
