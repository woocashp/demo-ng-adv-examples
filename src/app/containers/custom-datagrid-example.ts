import { Component, Input } from '@angular/core';

@Component({
    selector: 'tr[my-tr]',
    template: `
        <td *ngFor="let col of data | keyvalue">{{col.value}}</td>
        <td><button class="btn btn-secondary" (click)="go(data.id)">go</button></td>
    `
})
export class RowComponentType1 {
    @Input() data: any;
    go(evt) {
        console.log('go', evt);
    }
}

@Component({
    selector: 'tr[my-tr]',
    template: `
        <td class='bg-info p-1 text-white' *ngFor="let col of data | keyvalue">{{col.value}}</td>
    `
})

export class RowComponentType2 {
    @Input() data: any;
}

@Component({
    templateUrl: './custom-datagrid-examples.html'
})

export class CustomDataGridExampleComponent {
    type1: any;
    type2: any;
    data: any[] = [
        { title: 'tomato', phone: 123, id: 1000 },
        { title: 'pumpkin', phone: 567, id: 1001 }
    ];
    constructor() {
        this.type1 = RowComponentType1;
        this.type2 = RowComponentType2;
    }
    remove(item) {
        console.log('remove', item);
    }
    navigate(item) {
        console.log('navigate', item);
    }
}
