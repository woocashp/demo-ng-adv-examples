import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styles: [`
    .nav-link {border: 1px solid; margin: 0 3px;}
    `]
})
export class AppComponent { }
