import { CustomDataGridExampleComponent } from './containers/custom-datagrid-example';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RouterModule } from "@angular/router";
import { CustomIfExampleComponent } from "./containers/custom-if-example";
import { CustomTabsetExampleComponent } from "./containers/custom-tabs-example";
import { CustomForExampleComponent } from "./containers/custom-for-example";
import { ShareModule } from "./my-modules/share/share.module";
import { InitComponent } from './components/init/init.component';
import { CustomFormComponent } from './containers/custom-form.example';
import { MySelectComponent } from './my-modules/share/components/custom-form/my-select/my-select.component';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
    declarations: [
        AppComponent,
        InitComponent,
        CustomFormComponent,
        MySelectComponent,
        CustomIfExampleComponent,
        CustomForExampleComponent,
        CustomTabsetExampleComponent,
        CustomDataGridExampleComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterModule.forRoot([
            { path: 'home', component: InitComponent },
            { path: 'custom-if', component: CustomIfExampleComponent },
            { path: 'custom-for', component: CustomForExampleComponent },
            { path: 'custom-tabs', component: CustomTabsetExampleComponent },
            { path: 'data-grid', component: CustomDataGridExampleComponent },
            { path: 'custom-form', component: CustomFormComponent },
            { path: '**', redirectTo: 'home', pathMatch: 'full' }
        ]),
        ShareModule
    ],
    providers: [],
    bootstrap: [AppComponent],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class AppModule {
}
